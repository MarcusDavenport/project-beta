# CarCar

Team:

* Person 1 - Which microservice?
* Jason Tong - Service
Marcus - SALES

## Service microservice

* Jason Tong - Service



## Service microservice

The Service microservice is responsible for managing service appointments for automobiles and their owners. It consists of three main models: `Technician`, `AutomobileVO`, and `Appointment`.

`Technician` model represents the automobile technicians with fields for `first_name`, `last_name`, and `employee_id`.

`AutomobileVO` model represents the automobiles with fields for `vin` and `sold`.

`Appointment` model represents the service appointments with fields for `date_time`, `reason`, `status`, `vin`, `customer` and `technician`. The `technician` field is a foreign key linking to the `Technician` model, and the `vin` field is a CharField.

The Service microservice provides several API endpoints for managing technicians and appointments. These include endpoints for listing, creating, and deleting technicians and appointments, as well as endpoints for setting the status of an appointment to "canceled" or "finished".

In addition to the API, the Service microservice also includes an automobile poller that updates the `AutomobileVO` model every 60 seconds with updated VINs from the Inventory service.

The front-end of the Service microservice includes forms for adding a technician and creating a service appointment, as well as pages for listing all technicians and all service appointments. It also includes a special feature for marking appointments as "VIP" if the VIN provided for an appointment exists in our inventory, and a feature for canceling or finishing appointments.

Finally, the Service microservice includes a Service History page that shows the history of all service appointments and allows for searching of a particular VIN.

## Sales microservice

## Sales microservice

The Sales microservice is responsible for managing automobile sales. It consists of four main models: `Salesperson`, `Customer`, `Sale`, and `AutomobileVO`.

`Salesperson` model represents the salespeople with fields for `first_name`, `last_name`, and `employee_id`.

`Customer` model represents the customers with fields for `first_name`, `last_name`, `address`, and `phone_number`.

`Sale` model represents the sales with fields for `automobile`, `salesperson`, `customer`, and `price`. The `automobile`, `salesperson`, and `customer` fields are foreign keys linking to the `AutomobileVO`, `Salesperson`, and `Customer` models respectively.

`AutomobileVO` model represents the automobiles with fields for `vin` and `sold`.

The Sales microservice provides several API endpoints for managing salespeople, customers, and sales. These include endpoints for listing, creating, and deleting salespeople, customers, and sales.

In addition to the API, the Sales microservice also includes an automobile poller that updates the `AutomobileVO` model every 60 seconds with updated VINs from the Inventory service.

The front-end of the Sales microservice includes forms for adding a salesperson and a customer, and recording a new sale, as well as pages for listing all salespeople, all customers, and all sales. It also includes a special feature for only allowing sales of unsold automobiles from the inventory, and a feature for showing the sales history for a salesperson.
