from django.shortcuts import render
from .models import(
    AutomobileVO,
    Sale,
    Salesperson,
    Customer
)
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse,Http404


# Create your views here.

# ENCODERS:


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id","vin", "sold"]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["id","first_name", "last_name", "employee_id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["id","first_name", "last_name", "address", "phone_number"]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["id","price", "automobile", "salesperson", "customer"]
    encoders= {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }



@require_http_methods(["GET"])
def list_automobileVOs(request):
    if request.method =="GET":
        automobileVO = AutomobileVO.objects.all()
        if not automobileVO:
            raise Http404("No AutomobileVO objects exist")
        return JsonResponse(
            {"AutomobileVO": automobileVO},
            encoder=AutomobileVOEncoder,
        )


@require_http_methods(["GET","POST"])
def list_or_create_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        if not salespeople:
            raise Http404("No Salespeople exist.")
        return JsonResponse(
            {"salespeople": salespeople},
            encoder= SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False
            )
        except ValueError:
            return JsonResponse({"error": "Invalid JSON format"}, status=400)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=400)

@require_http_methods(["DELETE"])
def delete_salespeople(request, pk):
    try:
        salesperson = Salesperson.objects.get(id=pk)
        salesperson.delete()
        delete_message = {f"salesperson {salesperson.first_name} {salesperson.last_name}, employee #{salesperson.employee_id}": "has been deleted"}
        return JsonResponse(delete_message, safe=False)
    except Salesperson.DoesNotExist:
        return JsonResponse({"salesperson delete":"Failed. Does not exist"},status=404,
                            safe=False)


@require_http_methods(["GET","POST"])
def list_or_create_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        if not customer:
            raise Http404("No Customer objects exist.")
        return JsonResponse(
            {"customers": customer},
            encoder= CustomerEncoder,

        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except ValueError:
            return JsonResponse({"error": "Invalid JSON"}, status=400)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=400)

@require_http_methods(["DELETE"])
def delete_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
        customer.delete()
        delete_message = {f"customer {customer.first_name} {customer.last_name}": "has been deleted"}
        return JsonResponse(delete_message, safe=False)
    except Customer.DoesNotExist:
        return JsonResponse({"customer delete":"Failed. Does not exist"}, status=404,
                            safe=False)


@require_http_methods(["GET","POST"])
def list_or_create_sale(request):
    if request.method == "GET":
        sale = Sale.objects.all()
        if not sale:
            raise Http404("No Sales objects exist")
        return JsonResponse(
            {"sales": sale},
            encoder= SaleEncoder,
            safe=False

        )
    else:
        try:
            content = json.loads(request.body)

            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson

            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile

            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

            sale = Sale.objects.create(**content)

            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except ValueError:
            return JsonResponse({"error": "Invalid JSON"}, status=400)
        except (Salesperson.DoesNotExist, AutomobileVO.DoesNotExist, Customer.DoesNotExist):
            return JsonResponse({"sale create": "Failed - Does not exist"}, status=404, safe=False)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=400)

@require_http_methods(["DELETE"])
def delete_sale(request, pk):
    try:
        sale = Sale.objects.get(id=pk)
        sale.delete()
        delete_message = {f"sale record: vin# '{sale.automobile}' for ${sale.price}.00" : "deleted"}
        return JsonResponse(delete_message, safe=False)
    except Sale.DoesNotExist:
        return JsonResponse({"sale delete":"Failed. Does not exist"},status=404,
                            safe=False)
