import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


function SalespersonList() {
    const [salespeople, setSalespeople] = useState([]);
    const navigate = useNavigate()

    const fetchSalespeople = async () => {
        const salespeopleApiUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespeopleApiUrl);
        if (response.ok) {
            const data = await response.json();

            if (data === undefined) {
                return null;
            }

            setSalespeople(data.salespeople)
        }
    }
    useEffect(() => {
        fetchSalespeople();
    }, [])

    const handleOnClick = () => {
    navigate("/salespeople/create");
}

    return (
    <>
    <div>
      <h1>Salesperson List</h1>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>First name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(sp => {
            return (
              <tr key={ sp.employee_id }>
                <td>{sp.first_name}</td>
                <td>{ sp.last_name }</td>
                <td>{ sp.employee_id }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div><button onClick={handleOnClick} type="button" className='btn btn-primary' data-bs-toggle="button">add a new salesperson?</button></div>
      </div>
      </>
    );
  }

export default SalespersonList;
