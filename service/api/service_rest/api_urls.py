from django.urls import path
from .views import technician_delete
from .views import technician_api, appointment_api
from .views import check_automobileVO

urlpatterns = [
    # Technicians
    path("technicians/", technician_api, name="technician-list-create"),
    path("technicians/<int:pk>/", technician_delete, name="technician-delete"),
    path("technicians/<int:id>/", technician_api, name="technician-detail"),
    # Appointments
    path("appointments/", appointment_api, name="appointment-list-create"),
    path("appointments/<int:pk>/", appointment_api, name="appointment-detail"),
    path("appointments/<int:pk>/cancel/", appointment_api, name="appointment-cancel"),
    path("appointments/<int:pk>/finish/", appointment_api, name="appointment-finish"),
    path("appointments/vipcheck/", check_automobileVO, name="vipcheck"),
]
