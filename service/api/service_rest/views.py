import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
from django.views.decorators.http import require_http_methods

# Create your views here.


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "date_time",
        "reason",
        "customer",
        "status",
        "technician",
        "id",
    ]

    def default(self, obj):
        if isinstance(obj, Technician):
            return {
                "id": obj.id,
                "first_name": obj.first_name,
                "last_name": obj.last_name,
                "employee_id": obj.employee_id,
            }
        return super().default(obj)


@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def appointment_api(request, technician_id=None, pk=None):
    try:
        if request.method == "GET":
            if technician_id:
                technician = Technician.objects.get(id=technician_id)
                appointments = Appointment.objects.filter(technician=technician)
            else:
                appointments = Appointment.objects.all()

            serialized_appointments = AppointmentEncoder().encode(appointments)
            return JsonResponse(
                {"appointments": json.loads(serialized_appointments)},
                safe=False,
            )

        elif request.method == "POST":
            content = json.loads(request.body)
            technician_id = content.get("technician_id")

            if technician_id:
                technician = Technician.objects.get(id=technician_id)
                try:
                    appointment = Appointment.objects.create(
                        technician=technician, **content
                    )
                    return JsonResponse(
                        json.loads(AppointmentEncoder().encode(appointment)),
                        safe=False,
                    )
                except Exception as e:
                    return JsonResponse(
                        {"message": f"Error creating appointment: {str(e)}"},
                        status=400,
                    )
            else:
                return JsonResponse(
                    {
                        "message": "Assigning a technician is required to make an appointment."
                    },
                    status=400,
                )

        elif request.method == "PUT":
            content = json.loads(request.body)
            appointment_id = pk

            try:
                appointment = Appointment.objects.get(id=appointment_id)

                status = content.get("status")
                if status:
                    appointment.status = status

                appointment.save()

                return JsonResponse(
                    json.loads(AppointmentEncoder().encode(appointment)),
                    safe=False,
                )
            except Appointment.DoesNotExist:
                return JsonResponse(
                    {"message": "Appointment does not exist"},
                    status=404,
                )
            except Exception as e:
                return JsonResponse(
                    {"message": f"Error updating appointment: {str(e)}"},
                    status=400,
                )
            except Appointment.status not in ["finished", "canceled", "scheduled"]:
                return JsonResponse(
                    {"message": "Invalid response, status must be finished, canceled or scheduled"},
                    status=400
                )

        elif request.method == "DELETE":
            try:
                appointment = Appointment.objects.get(id=pk)
                appointment.delete()
                delete_message = {f" {appointment.customer}": "appointment has been deleted"}
                return JsonResponse(delete_message, safe=False)

            except Appointment.DoesNotExist:
                response = JsonResponse({"message": "Appointment does not exist"})
                response.status_code = 400
                return response

    except Technician.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Technician"},
            status=400,
        )
    except Exception as e:
        return JsonResponse(
            {"message": f"Error: {str(e)}"},
            status=500,
        )


@require_http_methods(["GET", "POST", "PUT"])
def technician_api(request, id=None):
    if request.method == "GET":
        if id is not None:
            try:
                technician = Technician.objects.get(id=id)
                return JsonResponse(
                    technician,
                    encoder=TechnicianEncoder,
                    safe=False,
                )
            except Technician.DoesNotExist:
                response = JsonResponse({"message": "Technician does not exist"})
                response.status_code = 404
                return response
        else:
            technicians = Technician.objects.all()
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianEncoder,
            )

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Exception as e:
            return JsonResponse(
                {"message": f"Error creating technician: {str(e)}"},
                status=400,
            )

    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=id)
            for key, value in content.items():
                setattr(technician, key, value)
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=404,
            )
        except Exception as e:
            return JsonResponse(
                {"message": f"Error updating technician: {str(e)}"},
                status=400,
            )


@require_http_methods(["DELETE"])
def technician_delete(request, pk):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                {"message": "Technician removed sucessfully."},
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response





@require_http_methods(["GET"])
def check_automobileVO(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"AutomobileVO": automobileVO},
            encoder=AutomobileVOEncoder,
        )
